# NEMO USC Nanofab plugin

To install the plugin, run
```
python -m pip install git+https://gitlab.com/nanofab_usc/nemo-usc-nanofab-plugin.git
```

Then add `"NEMO_usc_nanofab"` to INSTALLED_APPS **before** NEMO 

## Account administrators
* account administrators can be added in `detailed administration -> Accounts`
* they have the ability to:
  * create new projects under their accounts
  * edit existing projects
  * add/remove users to/from projects
