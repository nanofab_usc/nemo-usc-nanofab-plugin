from setuptools import setup, find_namespace_packages

setup(
    name="NEMO-usc-nanofab",
    version="1.2.0",
    packages=find_namespace_packages(),
    include_package_data=True,
)
