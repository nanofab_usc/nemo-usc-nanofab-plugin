from django.urls import path

from NEMO_usc_nanofab.views import accounts

urlpatterns = [
    path("administrator_accounts/projects/create/", accounts.edit_project, name="administrator_create_project"),
    path("administrator_accounts/projects/<int:project_id>/", accounts.edit_project, name="administrator_edit_project"),
    path("projects/<int:project_id>/toggle_active/", accounts.toggle_active_project, name="toggle_active_project"),
]
