from NEMO.models import Account, BaseModel, Project, User
from django.db import models
from django.dispatch import receiver


class AccountAdministrator(BaseModel):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    administrators = models.ManyToManyField(User, blank=False)


@receiver(models.signals.post_save, sender=Project)
def auto_add_admin_on_projects(sender, instance: Project, **kwargs):
    account_admins = AccountAdministrator.objects.filter(account=instance.account).values_list(
        "administrators", flat=True
    )
    for admin in account_admins:
        instance.manager_set.add(admin)
