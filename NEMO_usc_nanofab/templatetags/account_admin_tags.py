from django import template
from NEMO.models import Account, User

register = template.Library()


@register.simple_tag(takes_context=True)
def admin_for_account(context, account: Account):
    user: User = context["request"].user
    for admin_account in user.accountadministrator_set.all():
        if admin_account.account == account:
            return True
    return False
