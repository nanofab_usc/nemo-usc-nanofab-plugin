from NEMO.models import Account
from NEMO_usc_nanofab.models import AccountAdministrator
from django.contrib import admin
from django.utils.safestring import mark_safe


@admin.register(AccountAdministrator)
class AccountAdministratorAdmin(admin.ModelAdmin):
    list_display = ["account", "get_admins"]
    filter_horizontal = ["administrators"]

    @admin.display(description="Admins", ordering="administrators")
    def get_admins(self, account_admins: AccountAdministrator):
        return mark_safe("<br>".join(str(user) for user in account_admins.administrators.all()))

    def save_model(self, request, obj: AccountAdministrator, form, change):
        super().save_model(request, obj, form, change)
        if change:
            original_members = set(obj.administrators.all())
        else:  # The model object is being created (instead of changed) so we can assume there are no members (initially).
            original_members = set()
        current_members = set(form.cleaned_data["administrators"])
        account: Account = form.cleaned_data["account"]
        added_admins = []
        removed_admins = []

        # Log membership changes if they occurred.
        symmetric_difference = original_members ^ current_members
        if symmetric_difference:
            if change:  # the members have changed, so find out what was added and removed...
                # We can see the previous account admins by looking it up
                # in the database because the admin list hasn't been committed yet.
                added_admins = set(current_members) - set(original_members)
                removed_admins = set(original_members) - set(current_members)

            else:  # a model object is being created (instead of changed) so we can assume all the admins are new...
                added_admins = form.cleaned_data["administrators"]
        for user in added_admins:
            for project in account.project_set.all():
                project.manager_set.add(user)
        for user in removed_admins:
            for project in account.project_set.all():
                project.manager_set.remove(user)

    def delete_model(self, request, obj: AccountAdministrator):
        for user in obj.administrators.all():
            for project in obj.account.project_set.all():
                project.manager_set.remove(user)
        super().delete_model(request, obj)

    def delete_queryset(self, request, queryset):
        for obj in queryset:
            self.delete_model(request, obj)
