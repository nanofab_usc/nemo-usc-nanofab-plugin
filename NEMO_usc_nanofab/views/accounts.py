from NEMO.models import Account, ActivityHistory, MembershipHistory, Project, User
from NEMO_billing.invoices.models import ProjectBillingDetails
from NEMO_billing.invoices.views.project import ProjectDetailsForm
from NEMO_billing.rates.models import RateCategory
from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.views.decorators.http import require_POST, require_http_methods

from NEMO_usc_nanofab.models import AccountAdministrator


class CustomProjectForm(ModelForm):
    class Meta:
        model = Project
        exclude = ["only_allow_tools", "allow_consumable_withdrawals"]


@login_required
@require_http_methods(["GET", "POST"])
def edit_project(request, project_id=None):
    try:
        project = Project.objects.get(id=project_id)
    except (Project.DoesNotExist, ValueError):
        project = None
    try:
        project_details = project.projectbillingdetails
    except (ProjectBillingDetails.DoesNotExist, AttributeError):
        project_details = ProjectBillingDetails(project=project)
    url = (
        reverse("administrator_edit_project", args=[project_id])
        if project_id
        else reverse("administrator_create_project")
    )
    form = CustomProjectForm(request.POST or None, instance=project)
    details_form = ProjectDetailsForm(request.POST or None, instance=project_details)
    dictionary = {
        "account_list": Account.objects.filter(
            id__in=AccountAdministrator.objects.filter(administrators__in=[request.user]).values_list(
                "account", flat=True
            )
        ),
        "user_list": User.objects.filter(is_active=True),
        "rate_categories": RateCategory.objects.all(),
        "form": form,
        "form_url": url,
        "form_details": details_form,
    }
    if request.method == "GET":
        return render(request, "NEMO_usc_nanofab/edit_project.html", dictionary)
    elif request.method == "POST":
        if not form.is_valid() or not details_form.is_valid():
            return render(request, "NEMO_usc_nanofab/edit_project.html", dictionary)
        else:
            if form.cleaned_data["account"].id not in request.user.accountadministrator_set.values_list(
                "account", flat=True
            ):
                form.add_error("account", "You are not an administrator on this account")
                return render(request, "NEMO_usc_nanofab/edit_project.html", dictionary)
            project = form.save()
            details_form.instance.project = project
            details_form.save()
            active_changed = form.initial.get("active", None) != project.active
            account_changed = form.initial.get("account", None) != project.account.id
            if not project_id or account_changed:
                if project_id and account_changed:
                    removed_account_history = MembershipHistory()
                    removed_account_history.authorizer = request.user
                    removed_account_history.action = MembershipHistory.Action.REMOVED
                    removed_account_history.child_content_object = project
                    removed_account_history.parent_content_object = Account.objects.get(id=form.initial["account"])
                    removed_account_history.save()
                account_history = MembershipHistory()
                account_history.authorizer = request.user
                account_history.action = MembershipHistory.Action.ADDED
                account_history.child_content_object = project
                account_history.parent_content_object = project.account
                account_history.save()
            if not project_id or active_changed:
                project_history = ActivityHistory()
                project_history.authorizer = request.user
                project_history.action = project.active
                project_history.content_object = project
                project_history.save()

            return redirect("projects")


@require_POST
def toggle_active_project(request, project_id):
    project = get_object_or_404(Project, id=project_id)

    if project.account_id not in request.user.accountadministrator_set.values_list("account", flat=True):
        return HttpResponseBadRequest("You are not an administrator on this account")

    project.active = not project.active
    project.save()
    history = ActivityHistory()
    history.authorizer = request.user
    history.action = project.active
    history.content_object = project
    history.save()
    return redirect(request.META.get("HTTP_REFERER", "projects"))
