from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class NEMOAccountAdminConfig(AppConfig):
    name = "NEMO_usc_nanofab"
    default_auto_field = "django.db.models.AutoField"
    verbose_name = _("USC Nanofab")

    def ready(self):
        pass
